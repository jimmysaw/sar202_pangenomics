# SAR202_pangenomics

A collection of scripts and data files used in the SAR202 pangenomics paper

"data" folder contains results or metadata needed to create figures
"trees" folder contains single-gene trees and a phylogenomic tree
"scripts" folder contains scripts used to generate figures and/or to process data
